﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class Character
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Fullname { get; set; }
        [MaxLength(100)]
        public string Alias { get; set; }
        [MaxLength(50)]
        public string Gender { get; set; }
        public string PictureURL { get; set; }

        public ICollection<Movie> Movies { get; set; }

    }
}
