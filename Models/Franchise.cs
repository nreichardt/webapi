﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class Franchise
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(100)] 
        public string FranchiseName { get; set; }
        public string Description { get; set; }

        public ICollection<Movie> Movies { get; set; }
    }
}
