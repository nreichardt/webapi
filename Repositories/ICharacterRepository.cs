﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Models;

namespace WebAPI.Repositories
{
    public interface ICharacterRepository : IRepository<Character>
    {
        public IEnumerable<Character> GetAllCharactersByMovieId(int movieId);
        public IEnumerable<Character> GetAllCharactersByFranchiseId(int franchiseId);
        public bool AddCharacterToMovieByMovieId(int movieId, Character character);
        public bool RemoveCharacterFromMovieByMovieId(int movieId, int characterId);
    }
}
