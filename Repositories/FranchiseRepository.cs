﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Models;

namespace WebAPI.Repositories
{
    public class FranchiseRepository : IFranchiseRepository
    {
        private readonly MovieDbContext _context;

        public FranchiseRepository(MovieDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Franchise> GetAll()
        {
            return _context.Franchises.ToList();
        }

        public Franchise GetAllById(int id)
        {
            return _context.Franchises.Find(id);
        }

        public bool Add(Franchise entity)
        {
            _context.Franchises.Add(entity);
            bool result = _context.SaveChanges() > 0;

            return result;
        }

        public bool Update(Franchise entity)
        {
            _context.Franchises.Update(entity);
            bool result = _context.SaveChanges() > 0;

            return result;
        }

        public bool Delete(int id)
        {
            var toDelete = _context.Franchises.Find(id);
            if (toDelete == null) return false;
            _context.Franchises.Remove(toDelete);
            bool result = _context.SaveChanges() > 0;

            return result;
        }
    }
}
