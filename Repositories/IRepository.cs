﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Repositories
{
    public interface IRepository<T>
    {
        IEnumerable<T> GetAll();
        T GetAllById(int id);
        bool Add(T entity);
        bool Update(T entity);
        bool Delete(int id);
    }
}
