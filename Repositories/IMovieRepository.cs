﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Models;

namespace WebAPI.Repositories
{
    public interface IMovieRepository : IRepository<Movie>
    {
        public IEnumerable<Movie> GetAllMoviesByFranchiseId(int franchiseId);
        public bool AddMovieToFranchiseByFranchiseId(int franchiseId, Movie movie);
        public bool RemoveMovieFromFranchiseByMovieId(int franchiseId, int movieId);
    }
}
