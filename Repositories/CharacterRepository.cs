﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Models;

namespace WebAPI.Repositories
{
    public class CharacterRepository : ICharacterRepository
    {
        private readonly MovieDbContext _context;
        public CharacterRepository(MovieDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Character> GetAll()
        {
            return _context.Characters.ToList();
        }

        public Character GetAllById(int id)
        {
            return _context.Characters.Find(id);
        }

        public bool Add(Character entity)
        {
             _context.Characters.Add(entity);
            bool result = _context.SaveChanges() > 0;

            return result;
        }

        public bool Update(Character entity)
        {
            _context.Characters.Update(entity);
            bool result = _context.SaveChanges() > 0;

            return result;
        }

        public bool Delete(int id)
        {
            var toDelete = _context.Characters.Find(id);
            if (toDelete == null) return false;
            _context.Characters.Remove(toDelete);
            bool result = _context.SaveChanges() > 0;

            return result;
        }

        public IEnumerable<Character> GetAllCharactersByMovieId(int movieId)
        {
            return _context.Characters
                                .SelectMany(c => c.Movies, (c, m) => new { Character = c, MovieId = m.Id })
                                .Where(x => x.MovieId == movieId)
                                .Select(x => x.Character)
                                .ToList();
        }

        public IEnumerable<Character> GetAllCharactersByFranchiseId(int franchiseId)
        {
            // Works aswell
            //return _context.Characters
            //                    .SelectMany(c => c.Movies, (c, m) => new { Character = c, m.FranchiseId })
            //                    .Where(x => x.FranchiseId == franchiseId)
            //                    .Select(x => x.Character)
            //                    .Distinct()
            //                    .ToList();

            return _context.Characters
                                .Where(c => c.Movies.Select(m => m.FranchiseId).Contains(franchiseId))
                                .ToList();
        }

        // Should this be here ??
        //AddCharacterByMovieId - name if in Movie repo ?
        public bool AddCharacterToMovieByMovieId(int movieId, Character character)
        {
            var movie = _context.Movies.Find(movieId);
            if (movie == null) return false;

            movie.Characters.Add(character);
            bool result = _context.SaveChanges() > 0;

            return result;
        }

        public bool RemoveCharacterFromMovieByMovieId(int movieId, int characterId)
        {
            var movie = _context.Movies.Find(movieId);
            if (movie == null) return false;

            var characterToRemove = _context.Characters.Find(characterId);
            if (characterToRemove == null) return false;

            movie.Characters.Remove(characterToRemove);
            bool result = _context.SaveChanges() > 0;

            return result;
        }
    }
}
