﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Models;

namespace WebAPI.Repositories
{
    public class MovieRepository : IMovieRepository
    {
        private readonly MovieDbContext _context;
        public MovieRepository(MovieDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Movie> GetAll()
        {
            return _context.Movies.ToList();
        }

        public Movie GetAllById(int id)
        {
            return _context.Movies.Find(id);
        }

        public bool Add(Movie entity)
        {
             _context.Movies.Add(entity);
            bool result = _context.SaveChanges() > 0;

            return result;
        }

        public bool Update(Movie entity)
        {
            _context.Movies.Update(entity);
            bool result = _context.SaveChanges() > 0;

            return result;
        }

        public bool Delete(int id)
        {
            var toDelete = _context.Movies.Find(id);
            if (toDelete == null) return false;
            _context.Movies.Remove(toDelete);
            bool result = _context.SaveChanges() > 0;

            return result;
        }

        public IEnumerable<Movie> GetAllMoviesByFranchiseId(int franchiseId)
        {
            return _context.Movies.Where(m => m.FranchiseId == franchiseId).ToList();
        }

        public bool AddMovieToFranchiseByFranchiseId(int franchiseId, Movie movie)
        {
            var franchise = _context.Franchises.Find(franchiseId);
            if (franchise  == null) return false;

            franchise.Movies.Add(movie);
            bool result = _context.SaveChanges() > 0;

            return result;
        }

        public bool RemoveMovieFromFranchiseByMovieId(int franchiseId, int movieId)
        {
            var franchise = _context.Franchises.Find(franchiseId);
            if (franchise == null) return false;

            var movieToRemove = _context.Movies.Find(movieId);
            if (movieToRemove == null) return false;

            franchise.Movies.Remove(movieToRemove);
            bool result = _context.SaveChanges() > 0;

            return result;
        }
    }
}
